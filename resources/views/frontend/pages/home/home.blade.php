@extends('frontend.layout.frontendLayout')
@section('title', 'Home')
@section('content')

    <section class="wpb_row row-fluid section-padd">
        <div class="container">
            <div class="row">
                <div class="wpb_column column_container col-sm-12 col-md-12">
                    <div class="column-inner">
                        <div class="wpb_wrapper">
                            <div class="section-head ">
                                {{-- <h2><span>OUR SERVICES</span></h2> --}}
                                <h1 class="section-title">What we bring to you</h1>
                                <p>We provide expert tax and advisory services to individuals and small businesses. TAXAdviser optimizes tax costs using our years of experience.</p>
                            </div>

                            <div class="empty_space_30 md-hidden sm-hidden"></div>
                        </div>
                    </div>
                </div>

                <div class="wpb_column column_container col-sm-6 col-md-4">
                    <div class="column-inner">
                        <div class="wpb_wrapper">
                            <div class="service-box icon-box  ionic  hover-box">
                                <i class="ion-md-umbrella ion-logo-umbrella"></i>
                                <div class="content-box">
                                    <h6>Vocal Registration</h6>
                                    <p>We provide expert tax and advisory services to individuals and small businesses. TAXAdviser optimizes tax costs using our years of experience.</p>
                                    {{-- <a class="link-box pagelink" href="service-detail.html" target="_self">Read more</a> --}}
                                </div>
                            </div>

                            <div class="empty_space_30"></div>
                        </div>
                    </div>
                </div>
                <div class="wpb_column column_container col-sm-6 col-md-4">
                    <div class="column-inner">
                        <div class="wpb_wrapper">
                            <div class="service-box icon-box  ionic  hover-box">
                                <i class="ion-md-cube ion-logo-cube"></i>
                                <div class="content-box">
                                    <h6>Account Bookkeeping/Consultation</h6>
                                    <p>We provide expert tax and advisory services to individuals and small businesses. TAXAdviser optimizes tax costs using our years of experience.</p>
                                    {{-- <a class="link-box pagelink" href="service-detail.html" target="_self">Read more</a> --}}
                                </div>
                            </div>

                            <div class="empty_space_30"></div>
                        </div>
                    </div>
                </div>
                <div class="wpb_column column_container col-sm-6 col-md-4">
                    <div class="column-inner">
                        <div class="wpb_wrapper">
                            <div class="service-box icon-box  ionic  hover-box">
                                <i class="ion-md-podium ion-logo-podium"></i>
                                <div class="content-box">
                                    <h6>Company Liquidation</h6>
                                    <p>We provide expert tax and advisory services to individuals and small businesses. TAXAdviser optimizes tax costs using our years of experience.</p>
                                    {{-- <a class="link-box pagelink" href="service-detail.html" target="_self">Read more</a> --}}
                                </div>
                            </div>

                            <div class="empty_space_30"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="wpb_row row-fluid row-o-equal-height row-o-content-top row-flex relative">
        <div class="container">
            <div class="row">
                <div class="top-120 wpb_column column_container col-sm-12">
                    <div class="column-inner">
                        <div class="wpb_wrapper">
                            <div class="row wpb_row inner row-fluid row-o-equal-height row-o-content-top row-flex">
                                <div class="wpb_column column_container col-sm-12 col-md-4 col-has-fill bg-why-choose">
                                    <div class="column-inner"></div>
                                </div>
                                <div class="wpb_column column_container col-sm-12 col-md-8 col-has-fill">
                                    <div class="column-inner">
                                        <div class="wpb_wrapper">
                                            <div class="wpb_text_column wpb_content_element padding-box-3">
                                                <div class="wpb_wrapper">
                                                    <div class="section-head ">
                                                        <h6>OUR FEATURED</h6>
                                                        <h2 class="section-title">Why choose us?</h2>
                                                    </div>
                                                    <div class="gaps"></div>
                                                    <p>Roncus eu id tristique sollicitudin euismod vestibulum non lectus,
                                                        senectus conub purusnatoque neque ullamcorper cubilia faucibus, ac
                                                        eget ultrices hab aliquam molestie. Duis pulvinar ultricies accumsan
                                                        tortor platea.
                                                    </p>
                                                    <div>
                                                        <!-- contact info -->
                                                        <ul>
                                                            <li>
                                                                <i class="fa fa-clock-o"></i>
                                                                <span><strong>Best Idea</strong> <br><p>ffffffffffffffffffffffffffffffffffSuday Closed</p></span>
                                                            </li>
                                                            <li>
                                                                <i class="fa fa-map-marker"></i><span><strong>Communication</strong><br><p>fffffffffffffffffChamka mon, Phnom penh</p></span>
                                                            </li>
                                                              <li>
                                                                <i class="fa fa-phone"></i><span><strong>Confidence</strong><br><p>fffffffffffffffffffffffffffffmj@info.com.kh</p></span>
                                                            </li>
                                                        </ul>
                                                        <!-- contact info close -->
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--counter blog section-->

    <section class="wpb_row row-fluid mobile-padd section-counto row-has-fill">
        <div class="container">
            <div class="row">
                {{-- <div class="wpb_column column_container col-sm-12">
                    <div class="column-inner">
                        <div class="wpb_wrapper">
                            <div class="section-head  text-light">
                                <h6><span>achievements &amp; statistics</span></h6>
                                <h2 class="section-title">Our 25 years of operation</h2>
                            </div>

                            <div class="empty_space_12"></div>
                        </div>
                    </div>
                </div> --}}

                <div class="wpb_column col-sm-6 col-md-3">
                    <div class="column-inner">
                        <div class="wpb_wrapper">
                            <div class="fun-facts">
                                <div class="icon-fact">
                                    <i class="ion ion-md-ion ion-ios-people ion-logo-ion ion-ios-people"></i> </div>
                                <h4><span class="number text-primary bolder" data-to="350" data-speed="2000">0</span><br>in the world</h4>
                            </div>

                            <div class="empty_space_30 lg-hidden md-hidden"></div>
                        </div>
                    </div>
                </div>
                <div class="wpb_column col-sm-6 col-md-3">
                    <div class="column-inner">
                        <div class="wpb_wrapper">
                            <div class="fun-facts">
                                <div class="icon-fact">
                                    <i class="ion ion-md-ion ion-ios-document ion-logo-ion ion-ios-document"></i> </div>
                                <h4><span class="number text-primary bolder" data-to="1080" data-speed="2000">0</span><br>had finished</h4>
                            </div>

                            <div class="empty_space_30 lg-hidden md-hidden"></div>
                        </div>
                    </div>
                </div>
                <div class="wpb_column col-sm-6 col-md-3">
                    <div class="column-inner">
                        <div class="wpb_wrapper">
                            <div class="fun-facts">
                                <div class="icon-fact">
                                    <i class="ion ion-md-ion ion-ios-person ion-logo-ion ion-ios-person"></i> </div>
                                <h4><span class="number text-primary bolder" data-to="52" data-speed="2000">0</span> <br> are working</h4>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="wpb_column col-sm-6 col-md-3">
                    <div class="column-inner">
                        <div class="wpb_wrapper">
                            <div class="fun-facts">
                                <div class="icon-fact">
                                    <i class="ion ion-md-ion ion-ios-people ion-logo-ion ion-ios-people"></i> </div>
                                <h4><span class="number text-primary bolder" data-to="350" data-speed="2000">0</span> <br>in the world</h4>
                            </div>

                            <div class="empty_space_30 lg-hidden md-hidden"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--team section-->
    <section class="wpb_row row-fluid section-padd section-team">
        <div class="container">
            <div class="row">
                <div class="wpb_column column_container col-sm-12">
                    <div class="column-inner">
                        <div class="wpb_wrapper">
                            <div class="section-head ">
                                {{-- <h6><span>Our Experts</span></h6> --}}
                                <h2 class="section-title p-5">Our Management Team</h2>
                            </div>
                            <div class="empty_space_30"></div>
                        </div>
                    </div>
                </div>
                <div class="wpb_column column_container col-sm-6 col-md-3">
                    <div class="column-inner">
                        <div class="wpb_wrapper">
                            <div class="member-item radius">
                                <div class="avatar">
                                    <img src="https://mallofjapangroup.com/k-admin/uploads/image/42670(1).jpg" alt=""> <span class="overlay"></span>
                                    <div class="social-mem">
                                        <a href="#" target="_blank"><i class="fa fa-twitter"></i></a>
                                        <a href="#" target="_blank"><i class="fa fa-linkedin"></i></a>
                                        <a href="#" target="_blank"><i class="fa fa-facebook"></i></a>
                                    </div>
                                </div>
                                <div class="mem-info">
                                    <h5>Mr. Tomoaki Kobayashi<span class="font12 normal">Chief Executive Officer</span></h5>
                                    <hr>
                                    <p>
                                        Lorem ipsum dolor sit amet consectetur adipiscing elit ad, tincidunt

                                    </p>
                                </div>
                            </div>
                            <div class="empty_space_30"></div>
                        </div>
                    </div>
                </div>
                <div class="wpb_column column_container col-sm-6 col-md-3">
                    <div class="column-inner">
                        <div class="wpb_wrapper">
                            <div class="member-item radius">
                                <div class="avatar">
                                    <img src="https://mallofjapangroup.com/k-admin/uploads/image/member2.jpg" alt=""> <span class="overlay"></span>
                                    <div class="social-mem">
                                        <a href="#" target="_blank"><i class="fa fa-twitter"></i></a>
                                        <a href="#" target="_blank"><i class="fa fa-linkedin"></i></a>
                                        <a href="#" target="_blank"><i class="fa fa-facebook"></i></a>
                                    </div>
                                </div>
                                <div class="mem-info">
                                    <h5>Mr. Ket Vireak<span class="font12 normal">Executive Managing Director</span></h5>
                                    <hr>
                                    <p>
                                        Lorem ipsum dolor sit amet consectetur adipiscing elit ad, tincidunt

                                    </p>
                                </div>
                            </div>
                            <div class="empty_space_30"></div>
                        </div>
                    </div>
                </div>
                <div class="wpb_column column_container col-sm-6 col-md-3">
                    <div class="column-inner">
                        <div class="wpb_wrapper">
                            <div class="member-item radius">
                                <div class="avatar">
                                    <img src="https://mallofjapangroup.com/k-admin/uploads/image/26926.jpg" alt=""> <span class="overlay"></span>
                                    <div class="social-mem">
                                        <a href="#" target="_blank"><i class="fa fa-twitter"></i></a>
                                        <a href="#" target="_blank"><i class="fa fa-linkedin"></i></a>
                                        <a href="#" target="_blank"><i class="fa fa-facebook"></i></a>
                                    </div>
                                </div>
                                <div class="mem-info">
                                    <h5>Mr. Sim Makara<span class="font12 normal">General Manager</span></h5>
                                    <hr>
                                    <p>
                                        Lorem ipsum dolor sit amet consectetur adipiscing elit ad, tincidunt

                                    </p>
                                </div>
                            </div>
                            <div class="empty_space_30"></div>
                        </div>
                    </div>
                </div>
                <div class="wpb_column column_container col-sm-6 col-md-3">
                    <div class="column-inner">
                        <div class="wpb_wrapper">
                            <div class="member-item radius">
                                <div class="avatar">
                                    <img src="https://mallofjapangroup.com/k-admin/uploads/image/member14-1.jpg" alt=""> <span class="overlay"></span>
                                    <div class="social-mem">
                                        <a href="#" target="_blank"><i class="fa fa-twitter"></i></a>
                                        <a href="#" target="_blank"><i class="fa fa-linkedin"></i></a>
                                        <a href="#" target="_blank"><i class="fa fa-facebook"></i></a>
                                    </div>
                                </div>
                                <div class="mem-info">
                                    <h5>Ms. Sok Cheata<span class="font12 normal">Director</span></h5>
                                    <hr>
                                    <p>
                                        Lorem ipsum dolor sit amet consectetur adipiscing elit ad, tincidunt

                                    </p>
                                </div>
                            </div>
                            <div class="empty_space_30"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--blog post section-->
    <section class="wpb_row row-fluid section-padd section-news">
        <div class="container">
            <div class="row">
                <div class="wpb_column column_container col-sm-12 col-md-8">
                    <div class="column-inner">
                        <div class="wpb_wrapper">
                            <div class="row wpb_row inner row-fluid">
                                <div class="wpb_column column_container col-sm-12 col-md-8">
                                    <div class="column-inner">
                                        <div class="wpb_wrapper">
                                            <div class="section-head ">
                                                <h6><span>our blog</span></h6>
                                                <h2 class="section-title">Our latest news</h2>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                                <div class="wpb_column column_container col-sm-12 col-md-4">
                                    <div class="column-inner">
                                        <div class="wpb_wrapper">
                                            <div class="wpb_text_column wpb_content_element text-right mobile-left">
                                                <div class="wpb_wrapper">
                                                    <p><a class="pagelink gray" href="blog.html">View all posts</a></p>

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="empty_space_30 md-hidden sm-hidden"></div>
                        </div>
                    </div>
                </div>
                <div class="wpb_column column_container col-sm-12 col-md-8">
                    <div class="column-inner">
                        <div class="wpb_wrapper">
                            <div class="news-slider posts-grid row" data-show="2" data-auto="true">

                                <div>
                                    <article class="news-item content-area">
                                        <div class="inner-item radius-top">
                                            <div class="thumb-image">
                                                <a href="post.html">
                                                    <img src="https://via.placeholder.com/770x350" alt="">
                                                </a>
                                            </div>
                                            <div class="inner-post radius-bottom">
                                                <div class="entry-meta">
                                                    <span class="posted-on">
                                                        <time class="entry-date">September 11, 2017</time>
                                                    </span>
                                                    <span class="posted-in">
                                                        <a href="#">Consulting</a>
                                                    </span>
                                                </div>
                                                <h4 class="entry-title">
                                                    <a href="post.html">Solution financial for good startup</a>
                                                </h4>
                                                <p>
                                                    Lorem ipsum dolor sit amet consectetur adipiscing elit ad, tincidunt
                                                    senectus felis platea natoque mattis....
                                                </p>
                                                <a class="post-link" href="post.html">Read more</a>
                                            </div>
                                        </div>
                                    </article>
                                </div>

                                <div>
                                    <article class="news-item content-area">
                                        <div class="inner-item radius-top">
                                            <div class="thumb-image">
                                                <a href="post.html">
                                                    <img src="https://via.placeholder.com/770x350" alt="">
                                                </a>
                                            </div>
                                            <div class="inner-post radius-bottom">
                                                <div class="entry-meta">
                                                    <span class="posted-on">
                                                        <time class="entry-date">September 11, 2017</time>
                                                    </span>
                                                    <span class="posted-in">
                                                        <a href="#">Consulting</a>
                                                    </span>
                                                </div>
                                                <h4 class="entry-title">
                                                    <a href="post.html">Why Tech Should Behave More Like Finance</a>
                                                </h4>
                                                <p>
                                                    Lorem ipsum dolor sit amet consectetur adipiscing elit ad, tincidunt
                                                    senectus felis platea natoque mattis....
                                                </p>
                                                <a class="post-link" href="post.html">Read more</a>
                                            </div>
                                        </div>
                                    </article>
                                </div>

                                <div>
                                    <article class="news-item content-area">
                                        <div class="inner-item radius-top">
                                            <div class="thumb-image">
                                                <a href="post.html">
                                                    <img src="https://via.placeholder.com/770x350" alt="">
                                                </a>
                                            </div>
                                            <div class="inner-post radius-bottom">
                                                <div class="entry-meta">
                                                    <span class="posted-on">
                                                        <time class="entry-date">September 11, 2017</time>
                                                    </span>
                                                    <span class="posted-in">
                                                        <a href="#">Consulting</a>
                                                    </span>
                                                </div>
                                                <h4 class="entry-title">
                                                    <a href="post.html">Cutting Your Restaurant’s Operations Costs</a>
                                                </h4>
                                                <p>
                                                    Lorem ipsum dolor sit amet consectetur adipiscing elit ad, tincidunt
                                                    senectus felis platea natoque mattis....
                                                </p>
                                                <a class="post-link" href="post.html">Read more</a>
                                            </div>
                                        </div>
                                    </article>
                                </div>

                                <div>
                                    <article class="news-item content-area">
                                        <div class="inner-item radius-top">
                                            <div class="thumb-image">
                                                <a href="post.html">
                                                    <img src="https://via.placeholder.com/770x350" alt="">
                                                </a>
                                            </div>
                                            <div class="inner-post radius-bottom">
                                                <div class="entry-meta">
                                                    <span class="posted-on">
                                                        <time class="entry-date">September 11, 2017</time>
                                                    </span>
                                                    <span class="posted-in">
                                                        <a href="#">Consulting</a>
                                                    </span>
                                                </div>
                                                <h4 class="entry-title">
                                                    <a href="post.html">Solution financial for good startup</a>
                                                </h4>
                                                <p>
                                                    Lorem ipsum dolor sit amet consectetur adipiscing elit ad, tincidunt
                                                    senectus felis platea natoque mattis....
                                                </p>
                                                <a class="post-link" href="post.html">Read more</a>
                                            </div>
                                        </div>
                                    </article>
                                </div>

                                <div>
                                    <article class="news-item content-area">
                                        <div class="inner-item radius-top">
                                            <div class="thumb-image">
                                                <a href="post.html">
                                                    <img src="https://via.placeholder.com/770x350" alt="">
                                                </a>
                                            </div>
                                            <div class="inner-post radius-bottom">
                                                <div class="entry-meta">
                                                    <span class="posted-on">
                                                        <time class="entry-date">September 11, 2017</time>
                                                    </span>
                                                    <span class="posted-in">
                                                        <a href="#">Consulting</a>
                                                    </span>
                                                </div>
                                                <h4 class="entry-title">
                                                    <a href="post.html">Cutting Your Restaurant’s Operations Costs</a>
                                                </h4>
                                                <p>
                                                    Lorem ipsum dolor sit amet consectetur adipiscing elit ad, tincidunt
                                                    senectus felis platea natoque mattis....
                                                </p>
                                                <a class="post-link" href="post.html">Read more</a>
                                            </div>
                                        </div>
                                    </article>
                                </div>

                                <div>
                                    <article class="news-item content-area">
                                        <div class="inner-item radius-top">
                                            <div class="thumb-image">
                                                <a href="post.html">
                                                    <img src="https://via.placeholder.com/770x350" alt="">
                                                </a>
                                            </div>
                                            <div class="inner-post radius-bottom">
                                                <div class="entry-meta">
                                                    <span class="posted-on">
                                                        <time class="entry-date">September 11, 2017</time>
                                                    </span>
                                                    <span class="posted-in">
                                                        <a href="#">Consulting</a>
                                                    </span>
                                                </div>
                                                <h4 class="entry-title">
                                                    <a href="post.html">Why Tech Should Behave More Like Finance</a>
                                                </h4>
                                                <p>
                                                    Lorem ipsum dolor sit amet consectetur adipiscing elit ad, tincidunt
                                                    senectus felis platea natoque mattis....
                                                </p>
                                                <a class="post-link" href="post.html">Read more</a>
                                            </div>
                                        </div>
                                    </article>
                                </div>

                            </div>
                            <div class="empty_space_45 lg-hidden"></div>
                        </div>
                    </div>
                </div>
                <div class="wpb_column column_container col-sm-12 col-md-4">
                    <div class="column-inner">
                        <div class="wpb_wrapper">
                            <div class="wpb_text_column wpb_content_element text-light bg-second radius padd-box-29">
                                <div class="wpb_wrapper">
                                    <div class="section-head ">
                                        <h6>Subscribe</h6>
                                        <h3 class="section-title">Subscribe for<br> latest news</h3>
                                    </div>
                                    <p>Malesuada varius luctus urna duis placerat maecenas primis velit blandit.</p>
                                    <!-- Mailchimp for WordPress v4.5.2 - https://wordpress.org/plugins/mailchimp-for-wp/ -->
                                    <form class="mc4wp-form" method="post">
                                        <div class="mc4wp-form-fields">
                                            <div class="footer-sub">
                                                <input type="text" name="NAME" placeholder="Name" required="">
                                                <input type="email" name="EMAIL" placeholder="Email Address"
                                                    required="">
                                                <input type="submit" class="btn" value="Subscribe">
                                            </div>
                                        </div>
                                    </form>
                                    <!-- / Mailchimp for WordPress Plugin -->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
@section('scripts')

@endsection
