<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

	<title>Consultax – Financial & Consulting HTML5 Template</title>

	<link rel="stylesheet" id="bootstrap-css" href="css/bootstrap.css" type="text/css" media="all">
	<link rel="stylesheet" id="awesome-font-css" href="css/font-awesome.css" type="text/css" media="all">
	<link rel="stylesheet" id="ionicon-font-css" href="css/ionicon.css" type="text/css" media="all">
    <link rel="stylesheet" id="royal-preload-css" href="css/royal-preload.css" type="text/css" media="all">
	<link rel="stylesheet" id="slick-slider-css" href="css/slick.css" type="text/css" media="all">
	<link rel="stylesheet" id="slick-theme-css" href="css/slick-theme.css" type="text/css" media="all">
	<link rel="stylesheet" id="consultax-style-css" href="style.css" type="text/css" media="all">
     <!-- flag icon-->
     <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/flag-icon-css/6.6.3/css/flag-icons.min.css">
    <link rel="stylesheet" href="https://cdn.rawgit.com/michalsnik/aos/2.0.1/dist/aos.css" />
    <!-- MDB -->
    {{-- <link href="https://cdnjs.cloudflare.com/ajax/libs/mdb-ui-kit/4.2.0/mdb.min.css" rel="stylesheet"/> --}}
    <!-- Vendor CSS Files -->
    <link href="vendor/animate.css/animate.min.css" rel="stylesheet">
    <link href="vendor/aos/aos.css" rel="stylesheet">
    <link href="vendor/boxicons/css/boxicons.min.css" rel="stylesheet">
	<!-- RS5.0 Main Stylesheet -->
    <link rel="stylesheet" type="text/css" href="revolution/css/settings.css">

    <!-- RS5.0 Layers and Navigation Styles -->
    <link rel="stylesheet" type="text/css" href="revolution/css/layers.css">
    <link rel="stylesheet" type="text/css" href="revolution/css/navigation.css">

    <link rel="shortcut icon" href="favicon.png">
</head>
<body class="royal_preloader">
	<div id="page" class="site">

    @include('frontend.panel.header.header2')
    <!-- #site-header -->

    <div id="content" class="site-content">

        @include('frontend.panel.slider.slider2')
      @yield('content')

    </div>
    <!-- #content -->
     @include('frontend.panel.footer.footer')
    <!-- #site-footer -->
</div>

	<script type='text/javascript' src='js/jquery.min.js'></script>
	<script type='text/javascript' src='js/countto.min.js'></script>
	<script type='text/javascript' src='js/jquery.isotope.min.js'></script>
    <script type='text/javascript' src='js/slick.min.js'></script>
	<script type='text/javascript' src='js/royal_preloader.min.js'></script>
	<script type='text/javascript' src='js/scripts.js'></script>
	<script type='text/javascript' src='js/header-footer.js'></script>

    <!-- Vendor JS Files -->
    <script src="vendor/aos/aos.js"></script>
    <script src="https://cdn.rawgit.com/michalsnik/aos/2.0.1/dist/aos.js"></script>

    <!-- MDB -->
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/mdb-ui-kit/4.2.0/mdb.min.js"></script>

    <script type="text/javascript" src="revolution/js/jquery.themepunch.tools.min.js?rev=5.0"></script>
    <script type="text/javascript" src="revolution/js/jquery.themepunch.revolution.min.js?rev=5.0"></script>
    <!-- RS5.0 Extensions Files -->
    <script type="text/javascript" src="revolution/js/extensions/revolution.extension.video.min.js"></script>
    <script type="text/javascript" src="revolution/js/extensions/revolution.extension.slideanims.min.js"></script>
    <script type="text/javascript" src="revolution/js/extensions/revolution.extension.layeranimation.min.js"></script>
    <script type="text/javascript" src="revolution/js/extensions/revolution.extension.navigation.min.js"></script>
    <script type="text/javascript" src="revolution/js/extensions/revolution.extension.actions.min.js"></script>
    <script type="text/javascript" src="revolution/js/extensions/revolution.extension.kenburn.min.js"></script>
    <script type="text/javascript" src="revolution/js/extensions/revolution.extension.migration.min.js"></script>
    <script type="text/javascript" src="revolution/js/extensions/revolution.extension.parallax.min.js"></script>
	<script>
        jQuery(document).ready(function () {
            jQuery("#revolution-slider").revolution({
                sliderType: "standard",
                sliderLayout: 'fullscreen',
                delay: 7500,
                navigation: {
                    arrows: { enable: true }
                },
                spinner: "off",
                gridwidth: 1140,
                gridheight: 700,
                disableProgressBar: "on",
                responsiveLevels:[1920,1024,768,481],gridwidth:[1170,768,481,300],gridheight:[700,700,700,700]
            });
        });
        //
        $(function(){
        $('.selectpicker').selectpicker();
});
    </script>
</body>
</html>
