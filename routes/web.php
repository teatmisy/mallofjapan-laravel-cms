<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Frontend\FrontendController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
//frontend route

Route::get('/',[FrontendController::class,'index'])->name('homepage');
Route::get('/menu',[FrontendController::class,'menu'])->name('menu');
Route::get('/contact-us',[FrontendController::class,'contact_us'])->name('contact-us');
Route::get('/about-us',[FrontendController::class,'about_us'])->name('about-us');
Route::get('/service',[FrontendController::class,'service'])->name('service');
Route::get('/gallery',[FrontendController::class,'gallery'])->name('gallery');
Route::get('/blog',[FrontendController::class,'blog'])->name('blog');
Route::get('/blog/{id}',[FrontendController::class,'blog_detail'])->name('blog-detail');
Route::get('/poducts', [FrontendController::class, 'productList'])->name('products.list');
